const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const schema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    date: {
        type: Date,
        required: true
    },
    comment: {
        type: String
    },
    pluses: {
        type: Number
    },
    minuses: {
        type: Number
    },
    refunds: {
        type: Number
    },
    earning: {
        type: Number
    },
    screens: [
        {
            type:Schema.Types.ObjectId,
            ref: 'Screen'
        }
    ]
});


module.exports = Diary = mongoose.model('Diary', schema);