const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const fs = require('fs');

const schema = new Schema({
    path: {
        type: String,
        required: true
    }
})


schema.statics.saveSeveral = async function( arrayScreens ) {
    const Self = this;
    const idScreens = [];
    for(const screen of arrayScreens) {
        let newScreen = new Self( { path: screen.path } );
        newScreen = await newScreen.save();
        idScreens.push(newScreen._id);      
    }
    return idScreens;
}

schema.statics.removeScreen = async function( screen_id ) {
    const Self = this;
    try {
        const screen = await Self.findByIdAndRemove(screen_id);
        fs.unlink(screen.path, (err) => {
            if(err) throw err;
            console.log('file was remowed');
        })

    } catch(error) {
        console.log(error);
        throw error;

    }
}

module.exports = Screen = mongoose.model('Screen', schema);