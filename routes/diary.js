const express = require('express');
const router = express.Router();
const multer = require('multer');
const withAuth = require('../middlewares/jwtMiddleware');
const Diary = require('../database/models/diary/Diary');
const Sreen = require('../database/models/diary/Screen');
const User = require('../database/models/users/User');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './screens');
      },
      filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
      }
})

const upload = multer({
    limits: {fileSize: 1000000},
    storage: storage
  })


router.post('/createDiary', withAuth, upload.array('screens', 10), async function(req, res, next) {
    try {
      const { date, comment, pluses, minuses, refunds, earning } = req.body;
      console.log(req.body);

      const user = await User.findOne( { email: req.email } );
      const screens = await Screen.saveSeveral(req.files);

      let diary = new Diary( { user: user._id, screens: screens, date: new Date(date), comment, pluses, minuses, refunds, earning } )
      diary = await diary.save();
      diary = await Diary.find({user: user._id}).populate('screens');
      res.json(diary);

    } catch(error) {
        console.log('createDiary::: ', error);
        res.json( { error: 'проверьте введенные данные' } );
    }
})

router.post('/addScreens',withAuth, upload.array('screens', 10), async function(req, res, next) {
  try {
      const screens = await Screen.saveSeveral(req.files);
      let day = await Diary.findById(req.body.day_id);
      const allScreens = day.screens.concat(screens);
      day = await Diary.findByIdAndUpdate(req.body.day_id, {screens: allScreens}, {new: true}).populate('screens');
      res.json(day);
  }catch (e) {
      res.json( { error: 'проверьте введенные данные' } );
  }
})

router.post('/update',withAuth, async function(req, res, next) {
  try {
    let day = await Diary.findByIdAndUpdate(req.body.day_id, req.body, {new: true});
    console.log(day)
    res.json(day);
  } catch {
    console.log('updateDiary::: ', error);
        res.json( { error: 'проверьте введенные данные' } );
  }
})

router.get('/getDiary', withAuth, async function(req, res, next) {
    try{
      const user = await User.findOne( { email: req.email } );
      const diary = await Diary.find( { user: user._id } ).populate('screens');
      res.json(diary);
    } catch(error) {
      console.log(error);
      res.json({error: 'внутренная ошибка сервера'});
    }
})

router.get('/getDiaryDay', withAuth, async function(req, res, next) {
  try{
    const day = await Diary.findOne( {_id: req.query.day_id} ).populate('screens');
    res.json(day);
  } catch(error) {
    console.log(error);
    res.json({error: 'внутренная ошибка сервера'});
  }
})

router.get('/removeScreen', withAuth, async function(req, res, next) {
  try {
      let day = await Diary.findById(req.query.day_id);
      const screens = day.screens.filter((elem) => elem.toString() !== req.query.screen_id);
      await Screen.removeScreen(req.query.screen_id);
      day = await Diary.findByIdAndUpdate(req.query.day_id, {screens: screens}, {new: true}).populate('screens');
      res.json(day);
  } catch(e) {
      console.log(error);
      res.json({error: 'внутренняя ошибка сервера'});
  }
})

router.get('/removeDay', withAuth, async function(req, res, next) {
  try {
      const user = await User.findOne( { email: req.email } )
      const day = await Diary.findByIdAndRemove(req.query.day_id);
      for( const screen of day.screens) {
        await Screen.removeScreen( screen );
      }
      const diary = await Diary.find({user: user._id})
      res.json(diary)
  } catch(error) {
    console.log(error);
    res.json({error: 'внутренняя ошибка сервера'});
  }
})




module.exports = router;