var express = require('express');
var router = express.Router();
const User = require('../database/models/users/User');
const secret = require('../secret');
const jwt = require('jsonwebtoken');
const withAuth = require('../middlewares/jwtMiddleware');
const email = require('../helpres/EmailSend');
const generatePassword = require('password-generator');


router.post('/register', function(req, res, next) {
  const { email, password } = req.body;
  const user = new User({ email, password });
  user.save( function(err) {
    if(err) {
      res.status(500).json({error: 'пользователь уже существует'})
    }
    else res.json({ message: 'регистрация прошла успешно.' })
  })
});

router.post('/authenticate', async function( req, res, next ) {
    const { email, password } = req.body;
    try {
      const user = await User.findOne( { email: email } );
      console.log(user);
      if( !user ) {
        res.json( { error:'Неправильная почта или пароль' } );
      }
      else {
        user.isCorrectPassword(password, function(err, same) {
          if( err ) {
            res.json( { error: 'что-то пошло не так...' } );
          }
          else if( !same ) {
            res.json( { error: 'неправильная почта или пароль' } );
          }
          else {
            const payload = { email };
            const token = jwt.sign( payload, secret, {
              expiresIn: '7d'
            });
            res.cookie('token', token, { httpOnly: true } ).sendStatus(200);
          }
        })
      }

    } catch (e){
        console.log(e);
        res.json( { error: 'что-то пошло не так...' } );
    }
})

router.post('/updatePassword', async function(req, res, next) {
    const  password = generatePassword(8, false);
    let user = await User.findOne({email: req.body.email});
    if(user) {
      const removed = await User.remove({_id: user._id});
      user = new User({email: req.body.email, password: password});
      user = await user.save();
      let result = await email(req.body.email, password);
      res.sendStatus(200);
    }
    else {
      res.json({
        error: 'пользователя с таким email не существует.'
      })
    }
})


router.get('/logOut', function(req, res, next) {
    const token = req.cookies.token;
    res.clearCookie('token', token, { httpOnly: true } ).json({message: 'вы успешно вышли из системы.'});
})

router.get('/checkAuth',withAuth, function(req, res, next) {
  res.sendStatus(200);
})

module.exports = router;
