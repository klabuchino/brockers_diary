import { applyMiddleware, createStore, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import statisticsReducer from './main/reducers/statisticsReducer';
import earningsReducer from './main/reducers/earningsReducer';
import authReducer from './main/reducers/authReducer';
import diaryReducer from './main/reducers/diaryReducer';

const rootReducer = combineReducers({
    statistics: statisticsReducer,
    earnings: earningsReducer,
    auth: authReducer,
    diary: diaryReducer,
});


export default function configureStore() {
    const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
    return store;
};