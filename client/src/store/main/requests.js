import config from '../../config';
import axios from 'axios';

async function postRequest(url, data) {  
    return axios({
        method: 'post',
        url: config.urlServer + url,
        data: data,
        withCredentials: true
    }).then((res) => {
        return res.data;
    }).catch(err => {
        console.log(`error request::: ${err}`)
        throw err
    });
}

async function getRequest(url, params) {
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach((key) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);

    return axios({
        method: 'get',
        url: url_fetch,
        withCredentials: true
    }).then(resp => {
        return resp.data
    }).catch(err => {
        console.log(`error request::: ${err}`)
        throw err
    });
}


export {postRequest, getRequest};
