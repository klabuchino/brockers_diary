export default function fetchWrapper(dispatch, type, mainAction) {
    const loadingAction = {type: `${type}_LOADING`}
    dispatch(loadingAction);

    try {
        mainAction();
    } catch(e) {
        const errorAction = {type: `${type}_ERROR`, payload: e.message};
        dispatch(errorAction)
    }
}