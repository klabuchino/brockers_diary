import { GET_STATISTICS } from "../types";

const initalState = {
    loading: true,
    error: '',
    statistics: {}
};

export default function statisticsReducer(state = initalState, action) {
    switch(action.type) {
        case `${GET_STATISTICS}_SUCCESS`:
            if(action.payload.error) {
                return {
                    loading: false,
                    statistics: {},
                    error: action.payload.error
                };
            };
            return {
                loading: false,
                error: '',
                statistics: action.payload
            };
        case `${GET_STATISTICS}_ERROR`:
            return {
                loading: false,
                statistics: {},
                error: 'что-то пошло не так...'
            };
        case `${GET_STATISTICS}_LOADING`:
            return {
                loading: true,
                error: '',
                statistics: {}
            };

        case `${GET_STATISTICS}_CLEAR`: 
            return {
                loading: true,
                error: '',
                statistics: {}
            }
        default:
            return state;
    }
}