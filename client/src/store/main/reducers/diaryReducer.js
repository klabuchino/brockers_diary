import { GET_DIARY, GET_DIARY_DAY } from '../types';

const initalState = {
    days: [],
    day: {},
    error: false,
    loading: false
}

export default function diaryReducer(state = initalState, action) {
    switch(action.type) {
        case `${GET_DIARY}_SUCCESS`: {
            if(action.payload.error) {
                return {
                    days: [],
                    day: {},
                    error: action.payload.error,
                    loading: false
                }
            }
            return {
                days: action.payload.map( elem => {
                    return ({ ...elem, date: new Date(elem.date) });
                } ),
                day:{},
                error: false,
                loading: false
            }
        }
        case `${GET_DIARY}_ERROR`: {
            return {
                days: [],
                day: {},
                error: 'что-то пошло не так...',
                loading: false
            }
        }
        case `${GET_DIARY}_LOADING`: {
            return {
                days: [],
                day: {},
                error: false,
                loading: true
            }
        }
        case `${GET_DIARY_DAY}`: {
            if(action.payload.error) {
                return{
                    ...state,
                    day: {},
                    error: action.payload.error,
                    loading: false
                }
            }
            return {
                ...state,
                day: action.payload,
                error: false,
                loading: false
            }
        }
        default: return state;
    }
}