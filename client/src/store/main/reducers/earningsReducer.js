import { CALCULATE_EARNINGS } from '../types';

const initalState = {
    loading: false,
    error: false,
    earnings: 0
}

export default function earningsReducer(state = initalState, action) {
    switch(action.type) {
        case `${CALCULATE_EARNINGS}_SUCCESS`:
            return {
                error: false,
                loading: false,
                earnings: action.payload
            };
        case `${CALCULATE_EARNINGS}_ERROR`:
            return {
                loading: false,
                error: true,
                earnings: {}
            };
        case `${CALCULATE_EARNINGS}_LOADING`:
            return {
                loading: true,
                error: false,
                earnings: {}
            };
        default:
            return state;
    }
}