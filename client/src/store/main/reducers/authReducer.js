import { AUTH, LOGOUT, REGISTRATION } from '../types';


const initalState = {
    isAuth: false,
    error: false,
    message: '',
    loading: false
}

export default function authReducer(state = initalState, action) {
    switch(action.type) {
        
        case `${AUTH}_SUCCESS`:
            if(action.payload.error) {
                return {
                    isAuth: false,
                    error: true,
                    message: action.payload.error,
                    loading: false
                }
            }
            return {
                isAuth: true,
                error: false,
                message: '',
                loading: false
            };
        case `${AUTH}_ERROR`:
            return {
                isAuth: false,
                error: true,
                message: 'что-то пошло не так...',
                loading: false
            };
        case `${AUTH}_LOADING`:
            return {
                isAuth: false,
                error: false,
                message: '',
                loading: true
            };
        case LOGOUT: 
            return {
                isAuth: false,
                error: false,
                message: action.payload.message,
                loading: false
            }
        case REGISTRATION: 
        return {
            isAuth: false,
            error: false,
            message: action.payload.message,
            loading: false
        }


        default: return state;
    }
}