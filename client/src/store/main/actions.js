import { GET_STATISTICS, CALCULATE_EARNINGS, AUTH, LOGOUT, REGISTRATION, GET_DIARY, GET_DIARY_DAY } from "./types";
import { postRequest, getRequest } from './requests';
import fetchWrapper from './fetchWrapper';

export const getStatistics = (text, nameTrade) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_STATISTICS,
        async () => {
            const res = await postRequest('/getStatistics', { text: text, nameTrade: nameTrade } );
            
            const successAction = { type: `${GET_STATISTICS}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const clearStatisticsState = () => ({
    type: `${GET_STATISTICS}_CLEAR`,
    payload: {}
})

export const calculateEarnings = (sum, amount, percent, gapAmount, gap) => async dispatch => {
    fetchWrapper(
        dispatch,
        CALCULATE_EARNINGS,
        async () => {
            const res = await postRequest('/calculateEarnings', 
            {sum: sum, amount: amount, percent: percent, gapAmount: gapAmount, gap: gap} );
            const successAction = { type: `${CALCULATE_EARNINGS}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const auth = (email, password, cb) => async dispatch => {
    fetchWrapper(
        dispatch,
        AUTH,
        async () => {
            const res = await postRequest('/users/authenticate',{ email: email, password: password} );
            cb(res.error, res.message);
            const successAction = { type: `${AUTH}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const checkAuth = (email, password) => async dispatch => {
    fetchWrapper(
        dispatch,
        AUTH,
        async () => {
            const res = await getRequest('/users/checkAuth', []);
            const successAction = { type: `${AUTH}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const logOut = (cb) => async dispatch => {
    fetchWrapper(
        dispatch,
        LOGOUT,
        async () => {
            const res = await getRequest('/users/logOut', []);
           
            if(cb) cb(res.error, res.message);
            const successAction = { type: LOGOUT, payload: res };
            dispatch(successAction);
        }
    )
}

export const registration = (email, password, cb) => async dispatch => {
    fetchWrapper(
        dispatch,
        REGISTRATION,
        async () => {
            const res = await postRequest('/users/register', { email: email, password: password } );
            cb(res.error, res.message);
            const successAction = { type: REGISTRATION, payload: res };
            dispatch(successAction);
        }
    )
}

export const getDiary = () => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY,
        async () => {
            const res = await getRequest('/diary/getDiary', []);
            const successAction = { type: `${GET_DIARY}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const saveDiaryDay = (data) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY,
        async () => {
            const res = await postRequest('/diary/createDiary', data);
            const successAction = { type: `${GET_DIARY}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const removeDay = (day_id) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY,
        async () => {
            const res = await getRequest('/diary/removeDay', {day_id: day_id});
            const successAction = { type: `${GET_DIARY}_SUCCESS`, payload: res };
            dispatch(successAction);
        }
    )
}

export const getDiaryDay = (day_id, cb) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY_DAY,
        async () => {
            const res = await getRequest('/diary/getDiaryDay', {day_id: day_id});
            cb(res)
            const successAction = { type: `${GET_DIARY_DAY}`, payload: res };
            dispatch(successAction);
        }
    )
}

export const updateDay = (data, cb) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY_DAY,
        async () => {
            const res = await postRequest('/diary/update', data);
            cb(res)
            const successAction = { type: `${GET_DIARY_DAY}`, payload: res };
            dispatch(successAction);
        }
    )
}

export const removeScreen = (data) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY_DAY,
        async () => {
            const res = await getRequest('/diary/removeScreen', data);
            const successAction = { type: `${GET_DIARY_DAY}`, payload: res };
            dispatch(successAction);
        }
    )
}

export const addScreens = (data) => async dispatch => {
    fetchWrapper(
        dispatch,
        GET_DIARY_DAY,
        async () => {
            const res = await postRequest('/diary/addScreens', data);
            const successAction = { type: `${GET_DIARY_DAY}`, payload: res };
            dispatch(successAction);
        }
    )
}