export const GET_STATISTICS = 'GET_STATISTICS';
export const CALCULATE_EARNINGS = 'CALCULATE_EARNINGS';
export const AUTH = 'AUTH';
export const LOGOUT = 'LOGOUT';
export const REGISTRATION = 'REGISTRATION';
export const GET_DIARY = 'GET_DIARY';
export const GET_DIARY_DAY = 'GET_DIARY_DAY';