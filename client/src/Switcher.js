import React, { Component } from 'react';
import { connect } from 'react-redux';
import { checkAuth } from './store/main/actions';
import { Switch, Route, Redirect } from 'react-router-dom';
import { StatisticsPage } from './pages/StatisticsPage';
import { ProfitFixedAmountPage } from './pages/ProfitFixedAmountPage';
import { MoneyManagementPage } from "./pages/MoneyManagementPage";
import  BrokersDiaryPage  from "./pages/BrokersDiaryPage";
import DiaryDayPage from './pages/DiaryDayPage';
import { AuthenticatePage } from './pages/AuthenticatePage';
import CreateDiaryDayPage from './pages/CreateDiaryDayPage';
import UpdatePasswordPage  from './pages/UpdatePasswordPage';


class Switcher extends Component {

    componentDidMount() {
        this.props.checkAuth();
    }

    render () {
        return (
            <Switch>
                <Route exact path="/">
                    <StatisticsPage/>
                </Route>
                <Route path ="/profit">
                    <ProfitFixedAmountPage/>
                </Route>
                <Route path ="/management">
                    <MoneyManagementPage/>
                </Route>
                <Route path ="/diary">
                    <BrokersDiaryPage/>
                </Route>
                {
                    this.props.isAuth ? 
                    <Route path ="/diaryDay">
                        <DiaryDayPage/>
                    </Route> : null
                }
                {
                    this.props.isAuth ? 
                    <Route path ="/createDiaryDay">
                        <CreateDiaryDayPage/>
                    </Route> : null
                }
                {
                    !this.props.isAuth ? 
                    <Route path ="/updatePassword">
                        <UpdatePasswordPage/>
                    </Route> : null
                }
                { this.props.isAuth ? <Redirect push to='/' /> :
                    <Route path ="/auth">
                        <AuthenticatePage/>
                    </Route>
                }
            </Switch>
        );
    }
}

const mapStateToProps = state => ({
    isAuth: state.auth.isAuth
})

const mapDispatchToProps = dispatch => ({
    checkAuth: () => {
        dispatch(checkAuth())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Switcher);
