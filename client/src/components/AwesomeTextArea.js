import React from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import '../assets/css/awesomeTextArea.css'

export const AwesomeTextArea = props => {
    return (
        <div className='awesome_textarea'>
            <label htmlFor='textarea'>
                <div className='awesome_textarea_title'>{props.title}</div>
            </label>
            <TextareaAutosize 
                id='textarea' 
                className='awesome_textarea_input'                
                value={props.value}
                onChange={props.onChange}
            />
        </div>
    )
}
