import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AwesomeButton } from "../components/AwesomeButton";
import { postRequest } from '../store/main/requests';



class DiaryDayForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: []
        }
    }
    sendHandler = () => {
        const data = new FormData();
        this.state.files.forEach(file => {
            data.append('screens', file);
        })
        data.append('date', 'Wed May 13 2020 11:15:48 GMT+0500');
        postRequest('/diary/createDiary', data);
    }
    onChangeFileHandler = e => {
        let files = this.state.files;
        let newFiles = e.target.files
        for(let i = 0; i < newFiles.length; i++) {
            if(files.length < 10) {
                files.push(newFiles[i])
            }
        }
        this.setState( { files: files } )           
    }
    render() {
        return(
            <>
                <input type='file' name='screens' multiple accept='image/*' onChange={this.onChangeFileHandler} />
                <AwesomeButton onClick={this.sendHandler} />
            </>
        )
    }
}

export default connect()(DiaryDayForm);


