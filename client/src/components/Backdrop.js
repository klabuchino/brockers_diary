import React from "react";

import '../assets/css/backdrop.css';

export const Backdrop = props => (
    
    <div className={props.show ? "backdrop open" : "backdrop"} onClick={props.onClick}></div>
)