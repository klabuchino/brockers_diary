import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InputField } from "../components/InputField";
import { AwesomeButton } from "../components/AwesomeButton";
import { SelectField } from "../components/SelectField";
import { calculateEarnings } from "../store/main/actions";
import '../assets/css/ProfitForm.css';


class ProfitForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sum: 0,
            amount: 0,
            percent: 0,
            gapAmount: 0,
            gap: 'days'

        }
    }

    sendHandler = () => {
        this.props.calculateEarnings(
            this.state.sum,
            this.state.amount,
            this.state.percent,
            this.state.gapAmount,
            this.state.gap
        );
    }

    render() {
        let titleEarning = 'Возможная прибыль:';
        if(this.props.earnings.earnings < 0) titleEarning = 'Возможный убыток:';
        return(
            <>
                <div className='profit_form'>
                    <InputField 
                        label='Сумма сделки' 
                        value={this.state.sum} 
                        onChange={(e) => { this.setState( { sum: e.target.value } ) } } 
                        type='number'
                    />
                    <InputField 
                        label='Количество сделок в день' 
                        value={this.state.amount} 
                        onChange={(e) => { this.setState( { amount: e.target.value } ) } }  
                        type='number'
                    />
                    <InputField 
                        label='Процент прибыльных сделок'
                        value={this.state.percent} 
                        onChange={(e) => { this.setState( { percent: e.target.value } ) } } 
                        type='number'
                    />
                    <div className='input_with_select'>
                        <InputField 
                            label='Промежуток торговли' 
                            value={this.state.gapAmount} 
                            onChange={(e) => { this.setState( { gapAmount: e.target.value } ) } }  
                            type='number'
                        />
                        <SelectField 
                            className='select'
                            value={this.state.gap}
                            onChange={ (e) => { this.setState( { gap: e.target.value } ) } }
                        />
                    </div>
                    <AwesomeButton 
                        value='посчитать' 
                        className='profit_form__button'
                        onClick={this.sendHandler}
                    />
                </div>
                {
                    !this.props.earnings.error && !this.props.earnings.loading ?
                    <div className='profit_result'> {`${titleEarning} ${+(this.props.earnings.earnings).toFixed(1)}`} </div> :
                    null
                }
            </>
        )
    }
}
const mapStateToProps = state => ({
    earnings: state.earnings
})

const mapDispatchToProps = dispatch => ({
    calculateEarnings: (sum, amount, percent, gapAmount, gap) => {
        dispatch(calculateEarnings(sum, amount, percent, gapAmount, gap))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfitForm);
