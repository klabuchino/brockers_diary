import React from 'react';
import '../assets/css/inputField.css';


export const InputField = React.forwardRef((props, ref) => {
    let className = props.className ? 'input_field '+ props.className : 'input_field'
    className = props.error ? className + ' input_error' : className;
     
    return(
            <label className={className} ref={ref}>
                <div className='input_field__label'>
                    {props.label}
                </div>
                <input 
                    type={props.type} 
                    onFocus={props.onFocus}  
                    onBlur={props.onBlur} 
                    value={props.value} 
                    onChange={props.onChange}
                    maxLength = {props.maxLength}/>
            </label>
    )
});