import React, { Component } from 'react';

import Toolbar from "./Toolbar";
import SideDrawer from './SideDrawer';
import { Backdrop } from './Backdrop';

export class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {sideDriverOpen: false};
    };

    clickOnToggleButton = () => {
        this.setState( prevState => { 
            return { sideDriverOpen: !prevState.sideDriverOpen } 
        })
    }
    clickOnBackdrop = () => {
        this.setState({sideDriverOpen: false})
    }
    render() {
        return(
            <div style={{height: '100%'}}>
                <Toolbar clickOnToggleButton={this.clickOnToggleButton}/>
                <SideDrawer show={this.state.sideDriverOpen}/>
                <Backdrop show={this.state.sideDriverOpen} onClick={this.clickOnBackdrop}/>
            </div>
        )
    }
}