import React from "react";
import '../assets/css/Instruction.css';

export const Instruction = props => {
    return (
        <>
            <div className='instruction_title'> 
                <h1>{props.title}</h1>                        
            </div>
            <div className='instruction_text'>
                {props.text}
            </div>
        </>
    )
}