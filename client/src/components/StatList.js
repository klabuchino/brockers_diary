import React from 'react';
import '../assets/css/StatList.css'


export const StatList = props => {
    const { pluses, minuses, refunds, earning, percent } = props.stat;
    return (
        <div className = 'stat_list'>
            <div className = 'stat_list_elem'>
                Количество плюсов: 
                <div className='stat_num'>{pluses}</div>
            </div>
            <div className = 'stat_list_elem'>
                Количество минусов:   
                <div className='stat_num'>{minuses}</div>
            </div>
            <div className = 'stat_list_elem'>
                Количество возвратов: 
                <div className='stat_num'>{refunds}</div>
            </div>
            <div className = 'stat_list_elem'> 
                { +earning < 0 ? 'Убыток:' : 'Прибыль:' } 
                <div className={ +earning < 0 ? 'stat_num lession' : 'stat_num' }>{earning}</div>
            </div>
            {
                props.withPercent ? 
                <div className = 'stat_list_elem'> 
                    Процент прибыльных сделок: 
                    <div className={ +earning < 0 ? 'stat_num lession' : 'stat_num' }>{percent}</div>
                </div>
                : null
            }
        </div>
    )
}