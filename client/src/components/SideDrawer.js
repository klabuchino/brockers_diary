import React from "react";
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { logOut } from '../store/main/actions';
import '../assets/css/sideDrawer.css';
import youtube from '../assets/icons/youtube.png';
import vk from '../assets/icons/vk.png';
import telegram from '../assets/icons/telegram.png';


const SideDrawer = props => {
    let drawerClasses = 'side-driver'
    if(props.show) {
        drawerClasses = 'side-driver open'
    }

    const logOutHandler = () => {
        props.logOut();
    }

    return (
        <nav className={drawerClasses}>
            <ul>
            <li><NavLink exact to='/' className='side-driver__link'>Статистика сделок</NavLink></li>
            <li><NavLink to='/profit' className='side-driver__link'>Прибыль фиксой</NavLink></li>
            <li><NavLink to='/management' className='side-driver__link'>Калькулятор ступеней</NavLink></li>
            <li><NavLink to='diary' className='side-driver__link'>Дневник трейдера</NavLink></li>
            <li>
                                {
                                    props.isAuth ? 
                                    <div 
                                        className='side-driver__link'
                                        onClick={logOutHandler}
                                    >
                                        Выйти
                                    </div> :
                                    <NavLink to='/auth' className='side-driver__link'>Вход</NavLink>
                                }
                            </li>
            <li>
                <a href='https://www.youtube.com/channel/UCUjtYznCviM0TqDy2YV6YBw?view_as=subscriber' className='toolbar_link_servises'>
                    <img className='link_icon' src={youtube} alt=''/>
                    YOUTUBE КАНАЛ INVESTMENT START
                </a>
            </li>
            <li>
                <a href='https://vk.com/grigory_inv' className='toolbar_link_servises'>
                    <img className='link_icon' src={vk} alt=''/>
                    Наша группа
                </a>
            </li>
            <li>
                <a href='https://t.me/groupinv' className='toolbar_link_servises'>
                    <img className='link_icon' src={telegram} alt=''/>
                    Чат в telegram
                </a>
            </li>
        </ul>
        </nav>
    )
};

const mapStateToProps = state => ({
    isAuth: state.auth.isAuth
})

const mapDispatchToProps = dispatch => ({
    logOut: () => {
        dispatch(logOut());
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(SideDrawer);