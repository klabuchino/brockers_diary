import React, { Component } from 'react';
import { connect } from 'react-redux'
import { AwesomeButton } from '../components/AwesomeButton';
import { AwesomeTextArea } from '../components/AwesomeTextArea';
import { getStatistics } from '../store/main/actions';
import '../assets/css/statisticsForm.css';

class StatisticsForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            betInputValue: '',
            nameTrade: 'olympTrade',
            olympTrade: true,
            binomo: false,
            intradeBar: false
        }
    }

    buttonClearHandler = () => {
        this.setState({betInputValue: ''})
    }

    buttonSendHundler = () => {
        this.props.getStatistics(this.state.betInputValue, this.state.nameTrade);
        this.setState({betInputValue: ''});
    }
    render() {
        return(
            <div className='statistics_form'>
                <div className='statistics_form_buttons'>
                    <div className='three_buttons'>
                        <AwesomeButton 
                            className='statistics_form_button' 
                            active={this.state.olympTrade} 
                            value='Olymp trade'
                            onClick={e => this.setState({olympTrade:true, binomo: false, intradeBar: false, nameTrade: 'olympTrade'})}
                        />
                        <AwesomeButton 
                            className='statistics_form_button' 
                            active={this.state.binomo} 
                            value='Binomo'
                            onClick={e => this.setState({olympTrade:false, binomo: true, intradeBar: false, nameTrade: 'binomo'})}
                        />
                        <AwesomeButton 
                            className='statistics_form_button' 
                            active={this.state.intradeBar} 
                            value='Intrade bar'
                            onClick={e => this.setState({olympTrade:false, binomo: false, intradeBar: true,  nameTrade: 'intradeBar'})}
                        />
                    </div>
                    <div className='two_buttons'>
                        <AwesomeButton 
                            className='statistics_form_button' 
                            value='Посчитать' 
                            onClick={this.buttonSendHundler}
                        />
                        <AwesomeButton 
                            className='statistics_form_button' 
                            value='Удалить' 
                            onClick={this.buttonClearHandler}
                        />
                    </div>

                </div>
                <AwesomeTextArea 
                    title='Вставить скопированные сделки:'
                    value={this.state.betInputValue}
                    onChange={e=>this.setState({betInputValue: e.target.value})}
                />
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    getStatistics: (text, nameTrade) => {
        dispatch(getStatistics(text, nameTrade))
    }
})

export default connect(undefined, mapDispatchToProps)(StatisticsForm);