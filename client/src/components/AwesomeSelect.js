import React, { useState } from 'react';
import '../assets/css/AwesomeSelect.css'


export const AwesomeSelect = props => {
    const list = props.list ? props.list : [{}];

    const [selected, setSelected] = useState(list[0]);
    const [open, setOpen] = useState(false);

    const onClickElemHandler = (listElem) => {
        setSelected(listElem);
        setOpen(!open);
        if(props.onChange)
            props.onChange(listElem.value)
    }

    const onClickSelectedHandler = () => {
        setOpen(!open);
    }

    const mapList = () => 
        list.map((elem, index) => { 
            return (
                <div 
                    className='awesome_select__list_elem'
                    key={index.toString()}
                    onClick={ () => { onClickElemHandler( elem ) } }
                >
                    {elem.title}
                </div>
            )
        })
    const className = props.className ? 'awesome_select ' + props.className : 'awesome_select';
    return(
        <div className={className}>
            <div className='awesome_select__selected' onClick={ onClickSelectedHandler }>
                { selected.title }
            </div> 
            {
                open ? <div className='awesome_select__list'>{ mapList() }</div> : null
            }
            
        </div>
    )
}