import React, { useState, useEffect } from "react";
import { InputField } from './InputField';
import '../assets/css/MoneyManagementTable.css'


export const MoneyManagementTable = props => {
    //f - это fuctor - множитель значит 
    let level = props.level;
    const [f8, setF8] = useState(level);
    const [f2, setF2] = useState(level);
    const [f3, setF3] = useState(level);
    const [f4, setF4] = useState(level);
    const [f5, setF5] = useState(level);
    const [f6, setF6] = useState(level);
    const [f7, setF7] = useState(level);

    useEffect(() => {  
        const level = props.level
        setF2(level);
        setF3(level)
        setF4(level);
        setF5(level);
        setF6(level);
        setF7(level);
        setF8(level);
      }, [props.level]);


    const sum = props.sum ? props.sum : 0;
    const levelSums = [sum];
    const profits = [];


    const getLevelSum = (i, f) => {
        const sum = levelSums[i-1] * f;
        levelSums.push(sum);
        return +sum.toFixed(2);
    }

    const getProfit = (sum) => {
        const profit = sum * 0.8;
        profits.push(profit);
        return +profit.toFixed(2);
    }

    const getEarning = (i) => {
        let earning = profits[i];
        for(let k = 0; k < i; k++ ) {
            earning -= levelSums[k];
        }
        return +earning.toFixed(2);
    }
    return(
        <table className='money_management_table'>
            <thead>
                <tr>
                    <th className='level'>ступень</th>
                    <th>множитель</th>
                    <th>сумма сделки</th>
                    <th>доход от сделки</th>
                    <th>прибыль</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className='level'>1</td>
                    <td></td>
                    <td>{ sum }</td>
                    <td>{ +getProfit(sum).toFixed(2) }</td>
                    <td>{ +profits[0].toFixed(2) }</td>
                </tr>
                <tr>
                    <td className='level'>2</td>
                    <td><InputField type='number' value={f2} maxlength = "3" onChange = { e => setF2(e.target.value) }/></td>
                    <td>{getLevelSum(1, f2)}</td>
                    <td>{ getProfit(levelSums[1]) }</td>
                    <td>{ getEarning(1) }</td>
                </tr>
                <tr>
                    <td className='level'>3</td>
                    <td><InputField type='number' value={f3} onChange = { e => setF3(e.target.value) } /></td>
                    <td>{getLevelSum(2, f3)}</td>
                    <td>{ getProfit(levelSums[2]) }</td>
                    <td>{ getEarning(2) }</td>
                </tr>
                <tr>
                    <td className='level'>4</td>
                    <td><InputField type='number' value={f4} onChange = { e => setF4(e.target.value) } /></td>
                    <td>{getLevelSum(3, f4)}</td>
                    <td>{ getProfit(levelSums[3]) }</td>
                    <td>{ getEarning(3) }</td>
                </tr>
                <tr>
                    <td className='level'>5</td>
                    <td><InputField type='number' value={f5} onChange = { e => setF5(e.target.value) } /></td>
                    <td>{getLevelSum(4, f5)}</td>
                    <td>{ getProfit(levelSums[4]) }</td>
                    <td>{ getEarning(4) }</td>
                </tr>
                <tr>
                    <td className='level'>6</td>
                    <td><InputField type='number' value={f6} onChange = { e => setF6(e.target.value) } /></td>
                    <td>{getLevelSum(5, f6)}</td>
                    <td>{ getProfit(levelSums[5]) }</td>
                    <td>{ getEarning(5) }</td>
                </tr>
                <tr>
                    <td className='level'>7</td>
                    <td><InputField type='number' value={f7} onChange = { e => setF7(e.target.value) } /></td>
                    <td>{getLevelSum(6, f7)}</td>
                    <td>{ getProfit(levelSums[6]) }</td>
                    <td>{ getEarning(6) }</td>
                </tr>
                <tr>
                    <td className='level'>8</td>
                    <td><InputField type='number' value={f8} onChange = { e => setF8(e.target.value) } /></td>
                    <td>{getLevelSum(7, f8)}</td>
                    <td>{ getProfit(levelSums[7]) }</td>
                    <td>{ getEarning(7) }</td>
                </tr>
            </tbody>
        </table>
    )
}