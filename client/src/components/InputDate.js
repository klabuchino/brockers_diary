import React, {Component } from 'react';
import DayPicker from '../components/DayPicker/DayPicker';
import { InputField } from '../components/InputField';
import '../assets/css/InputDate.css';

const monthNames = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];
const longDayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда' , 'Четвег', 'Пятница', 'Суббота'];
const shortDayNames = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб']


export class InputDate extends Component {
    constructor(props) {
        super(props);
        this.daypicker = React.createRef();
        this.input = React.createRef();
        this.state = {
            focus: false,
            open: false
        }
    }


    onDayClickHandler = day => {
        const dateStr = twoNumbers( day.getDate().toString() ) + '.' 
            + twoNumbers( (day.getMonth() + 1).toString() ) + '.' 
            + day.getFullYear().toString();
        this.props.onChange(dateStr);
    }

    onChangeInputHandler = (e) => {
        this.props.onChange(e.target.value);
    }

    componentDidUpdate() {
        if(this.state.focus) {
            this.daypicker.current.focus();
        }
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }
    
    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }
    
    handleClickOutside = event => {
        const daypicker = this.daypicker.current;
        const input = this.input.current;
    
        if (!(daypicker || input) || !(daypicker.contains(event.target) || (input.contains(event.target)))) {
            this.setState({
                open: false
            });
        }
    }

    render() {
        const { label, error, className, value } = this.props;
        return (
        <div className = {className}>
            <InputField 
                ref={this.input}
                value={value}  
                onChange={this.onChangeInputHandler}
                label = { label }
                error = { error }
                onFocus = { () => {  
                    this.setState({ open: true }) } 
                }
            />
            <div 
                ref = {this.daypicker} 
                tabIndex='1'
                className={ this.state.open ? 'input_date_calendar' : 'close_calendar'}
                onClick = { () =>  this.setState({ focus: true })}
                onBlur = { () => this.setState({ focus: false, open: false }) }
            >          
                <DayPicker
                    monthNames={monthNames}
                    longDayNames={longDayNames}
                    shortDayNames={shortDayNames}
                    onDayClick={ this.onDayClickHandler }
                    active={[]}
                />
            </div>
        </div>
        )
    }
}

const twoNumbers = (numStr) => {
    if(numStr.length === 1) {
        return '0' + numStr;
    }
    return numStr
}