import React from 'react';
import '../assets/css/awesomeButton.css';

export const AwesomeButton = props => {
    let classNameButon = 'awesome-button ' + props.className
    if(props.active) {
        classNameButon = classNameButon + ' active_button'
    }
    return (
        <button className={classNameButon} onClick={props.onClick}>
            <div className='awesome-button__value'>
                {props.value}
            </div>
        </button>
    )
}