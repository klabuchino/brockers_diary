import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearStatisticsState } from '../store/main/actions';
import { Message } from './Message';
import '../assets/css/statisticsTable.css';

class StatisticsTable extends Component {

    componentDidUpdate(prevProps) {
        if(this.props.statistics.error && prevProps.statistics.error !== this.props.statistics.error) {
            this.timerId = setTimeout( () => { this.props.clearStatisticsState() }, 4000);
        }
        else {
            clearTimeout(this.timerId);
        }
    }

    render() {
        if(!this.props.statistics.loading && !this.props.statistics.error) {
            const earningsName = this.props.statistics.statistics.earnings < 0 ? 'убыток' : 'прибыль';
            return(
                <>
                    <div className='statistics_percent'>{`${this.props.statistics.statistics.percentProfit}% прибыльных сделок.`}</div>
                    <table className='statistics_table'>
                        <thead>
                            <tr>
                                <th>плюсы</th>
                                <th>минуса</th>
                                { this.props.statistics.statistics.refunds > 0 ? <th>возвраты</th> : null }
                                <th>{earningsName}</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td aria-label='плюсы'>{ this.props.statistics.statistics.pluses }</td>
                                <td aria-label='минуса'>{ this.props.statistics.statistics.minuses }</td>
                                { this.props.statistics.statistics.refunds > 0 ? <td aria-label='возвраты'>{this.props.statistics.statistics.refunds}</td> : null }
                                <td aria-label={earningsName}>{ Math.abs(this.props.statistics.statistics.earnings) }</td>
                            </tr>
                        </tbody>
                    </table>
                </>
            )
        }
        return (
            <Message value={this.props.statistics.error} error={true}/>
        )
    }
}

const mapStateToProps = state => ({
    statistics: state.statistics
})

const mapDispatchToProps = dispatch => ({
    clearStatisticsState: () => {
        dispatch(clearStatisticsState());
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(StatisticsTable);
