import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { auth, registration } from '../store/main/actions';
import '../assets/css/AuthForm.css';
import { InputField } from '../components/InputField';
import { AwesomeButton } from '../components/AwesomeButton';
import { Message } from '../components/Message';


class AuthForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isValidEmail: false,
            password: '',
            isRegistration: false,
            message: '',
            error: false
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.state.message && prevState.message !== this.state.message) {
            this.timerId = setTimeout(() => {this.setState({ message: '', err: false })}, 3000);
        }
        
    }
    onChangeEmailHandler = (e) => {
        let isValidEmail = false;
        // eslint-disable-next-line
        if(e.target.value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            isValidEmail = true;
        }
        this.setState( { email: e.target.value, isValidEmail: isValidEmail } )
    }

    authHandler = () => {
        const { email, isValidEmail, password, isRegistration } = this.state;
        if(isValidEmail){
            if(isRegistration) {
                this.props.registration(email, password, (err, message) => {
                    if(err) {
                        this.setState( { message: err, err: true } );
                    }
                    else {
                        this.setState( { message: message } );
                    }
                });
                this.setState({ isRegistration: false })
            }
            else {
                this.props.logIn(email, password, (err, message) => {
                    if(err) {
                        this.setState( { message: err, error: true } );
                    }
                    else {
                        this.setState( { message: message } );
                    }
                });
            }
        }
    }

    render() {
        const { isRegistration, isValidEmail, email, password, message, error } = this.state;
        return(
            <div className='auth_form'>
                <div className='header'>{ isRegistration ? 'Регистрация' : 'Вход' }</div>
                <InputField
                    label='email'
                    type='text'
                    className='auth_form__input'
                    error={!isValidEmail}
                    value={ email }
                    onChange = {this.onChangeEmailHandler}
                />
                <InputField
                    label='Пароль'
                    type='password'
                    className='auth_form__input'
                    value={ password }
                    onChange = {(e) => {this.setState( { password: e.target.value } ) } }
                />
                <AwesomeButton 
                    value= { isRegistration ? 'зарегестрироваться' : 'войти' }
                    className='auth_form__button' 
                    onClick={this.authHandler}
                />
                <AwesomeButton 
                    value={ isRegistration ? 'вход' : 'регистрация'  }
                    className='auth_form__button registration'
                    onClick={ () => { this.setState( { isRegistration: !isRegistration } ) } }
                />
                <Link className = 'link_update_pass' to='/updatePassword'>Забыли пароль?</Link>
                <Message 
                    value={message}
                    error={error}
                />
            </div>
        )
    }
}
const mapStateToProps = state => ({
    isAuth: state.auth.isAuth,
    message: state.auth.message
})

const mapDispatchToProps = dispatch => ({
    logIn: (email, password, cb) => {
        dispatch( auth( email, password, cb ) );
    },
    registration: (email, password, cb) => {
        dispatch( registration( email, password, cb));
    }
})

const AuthFormRoute = withRouter(AuthForm);
export default connect(mapStateToProps, mapDispatchToProps)(AuthFormRoute);

