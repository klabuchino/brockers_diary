import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logOut } from '../store/main/actions';
import { NavLink } from 'react-router-dom';
import "../assets/css/toolbar.css";
import youtube from '../assets/icons/youtube.png';
import vk from '../assets/icons/vk.png';
import telegram from '../assets/icons/telegram.png';
import logo from '../assets/icons/logo.png';
import { DrawerToggleButton } from "./DrawerToggleButton";  

class Toolbar extends Component {
    logOutHandler = () => {
        this.props.logOut();
    }
    render() {
        return (
            <header className='toolbar'>
                <nav className='toolbar__navigation'>
                    <div>
                        <DrawerToggleButton onClick={this.props.clickOnToggleButton}/>
                    </div>
                    <div className="toolbar__logo">
                        <img src={logo} alt=''/>
                        INVESTMENT START
                    </div>
                    <div className='spacer'></div>
                    <div className="toolbar_navigation_items">
                        <ul>
                            <li>
                                <a href='https://www.youtube.com/channel/UCUjtYznCviM0TqDy2YV6YBw?view_as=subscriber' className='toolbar_link_servises'>
                                    <img className='link_icon youtube' src={youtube} alt=''/>
                                    YOUTUBE КАНАЛ
                                </a>
                            </li>
                            <li>
                                <a href='https://vk.com/grigory_inv' className='toolbar_link_servises'>
                                    <img className='link_icon' src={vk} alt=''/>
                                    Наша группа
                                </a>
                            </li>
                            <li>
                                <a href='https://t.me/groupinv' className='toolbar_link_servises'>
                                    <img className='link_icon' src={telegram} alt=''/>
                                    Чат в telegram
                                </a>
                            </li>
                            <li><NavLink exact to='/' className='toolbar_link'>Статистика сделок</NavLink></li>
                            <li><NavLink to='/profit' className='toolbar_link'>Прибыль фиксой</NavLink></li>
                            <li><NavLink to='/management' className='toolbar_link'>Калькулятор ступеней</NavLink></li>
                            <li><NavLink to='/diary' className='toolbar_link'>Дневник трейдера</NavLink></li>
                            <li>
                                {
                                    this.props.isAuth ? 
                                    <div 
                                        className='toolbar_link'
                                        onClick={this.logOutHandler}
                                    >
                                        Выйти
                                    </div> :
                                    <NavLink to='/auth' className='toolbar_link'>Вход</NavLink>
                                }
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    isAuth: state.auth.isAuth
})

const mapDispatchToProps = dispatch => ({
    logOut: () => {
        dispatch(logOut());
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);