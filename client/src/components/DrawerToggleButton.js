import React from "react";
import '../assets/css/drawerToggleButton.css';

export const DrawerToggleButton = props => (
    <button className="toggle-button" onClick={props.onClick}>
        <div className="toggle-button__line"></div>
        <div className="toggle-button__line"></div>
        <div className="toggle-button__line"></div>
    </button>
);