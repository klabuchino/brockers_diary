import React, { useState } from 'react';
import { Backdrop } from '../components/Backdrop';
import '../assets/css/AwesomeImg.css'

export const AwesomeImg = props => {
    const [open, setOpen] = useState(false);
    const clickImgHandler = () => {
        setOpen(!open);
    }
    const clickOnBackdropHandler = () => {
        setOpen(false);
    }
    return (
        <>
            <div className={ open ? 'img_container' : '' } onClick={clickImgHandler}>
                <img src={props.src} alt = {props.alt} className={open ? `img_open`: props.className} onClick={clickImgHandler}/>
            </div>
            <Backdrop show={open} onClick={clickOnBackdropHandler} />
        </>
    )
}
