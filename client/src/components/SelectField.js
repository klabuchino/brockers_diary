import React from "react";
import '../assets/css/SelectField.css';

//по хорошему сделать его универсальным, но дальше мне он вряд ли пригодится так что оставлю так. надеюсь я не пожалею))))
export const SelectField = props => {
    const className = props.className ? `select_field ${props.className}` : 'select_field'
    return (
        <select className={className} value={props.value} onChange={props.onChange}>
            <option value='days'>Дней</option>
            <option value='months'>Месяцев</option>
            <option value='years'>Лет</option>
        </select>
    )
}