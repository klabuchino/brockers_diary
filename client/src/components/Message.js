import React, { Component } from 'react';
import '../assets/css/Message.css';


export class Message extends Component {

    className = () => {
        let className = 'message';
        if(this.props.error) {
            className = className + ' message_error';
        }
        if(this.props.value) {
            className = className + ' visible';
        }
        return className;
    }


        render() {
            return(
                <div className={this.className()}>
                    {this.props.value}
                </div>
            )
        }
}