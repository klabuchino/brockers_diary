import React from 'react';
import '../assets/css/BigDate.css';

const monthNames = [
    'Января',
    'Февраля',
    'Марта',
    'Апреля',
    'Мая',
    'Июня',
    'Июля',
    'Августа',
    'Сентября',
    'Октября',
    'Ноября',
    'Декабря'
];

const getNameDate = (date) => {
    const dateObj = new Date(date);
    const month = dateObj.getMonth();
    const day = dateObj.getDate().toString();
    return day + ' ' + monthNames[month];

}

export const BigDate = props => {
    const day = props.day;
    const date = getNameDate(day.date);
    return(
        <div className='big_date' onClick={props.onClick}>
            <div className='date'>{date}</div>
            <div className='big_date__statistics'>{day.pluses}+</div>
            <div className='big_date__statistics'>{day.minuses}-</div>
            <div className='big_date__earnings'>{ +day.earning < 0 ? 'убыток' : 'прибыль' }: {day.earning}</div>
        </div>
    )
}


