import React, { Component } from 'react';
import { Menu } from '../components/Menu';
import { MoneyManagementTable } from "../components/MoneyManagementTable";
import { Instruction } from "../components/Instruction";
import { InputField } from '../components/InputField';
import { AwesomeSelect } from '../components/AwesomeSelect';
import '../assets/css/MoneyManagementPage.css'


export class MoneyManagementPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sum: '',
            level: 2
        }
    }
    render() {
        return(
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <div className='money_managment_input'>
                            <InputField 
                                className='money_managment_input_field' 
                                type='number'
                                label='Сумма сделки' 
                                onChange={ ( e ) => { this.setState( { sum: e.target.value } ) } }
                                maxLength = '5'
                            />
                            <div className='money_managment_select_container'>
                                <AwesomeSelect className='money_managment_select'
                                    onChange={ ( value ) => { this.setState( { level: value } ) } }
                                    list={ [
                                        {value: 2, title: 'безопасный (2x)'},
                                        {value: 2.5, title: 'умеренный (2.5x)'},
                                        {value: 3, title: 'агрессивный (3x)'}
                                    ] }
                                />
                            </div>
                            
                        </div>
                        <MoneyManagementTable sum={this.state.sum} level={this.state.level}/>
                    </div>
                    <div className='content__right'>
                        <Instruction
                            title='Инструкция по подсчету'
                        />
                        <ul className = 'instruction_list'>
                            <li>Введите сумму сделки.</li>
                            <li>Выберите один из предложенных множителей мартингейла.</li>
                            <li>Также вы можете поменять значения множителя для каждой ступени.</li>
                        </ul>
                    </div>
                </div>
            </>
        );
    }
}