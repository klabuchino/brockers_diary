import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Menu } from '../components/Menu';
import { AwesomeButton } from '../components/AwesomeButton';
import { AwesomeTextArea } from '../components/AwesomeTextArea';
import { InputField } from '../components/InputField';
import { saveDiaryDay } from '../store/main/actions';
import '../assets/css/CreateDiaryDayPage.css'

const monthNames = [
    'Января',
    'Февраля',
    'Марта',
    'Апреля',
    'Мая',
    'Июня',
    'Июля',
    'Августа',
    'Сентября',
    'Октября',
    'Ноября',
    'Декабря'
];

class CreateDiaryDayPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            comment: '',
            pluses: 0,
            minuses: 0,
            refunds: 0,
            earning: 0
        }
    }
    saveHandler = () => {
        const { files, comment, pluses, minuses, refunds, earning } = this.state;
        const data = new FormData();
        files.forEach(file => {
            data.append('screens', file);
        })
        data.append('date', this.props.location.state.date);
        data.append('comment', comment)
        data.append('pluses', pluses)
        data.append('minuses', minuses)
        data.append('refunds', refunds)
        data.append('earning', earning)
        
        this.props.saveDiaryDay(data);

        this.props.history.push('/diary');
    }

    getNameDate = (date) => {
        const dateObj = new Date(date);
        const year = dateObj.getFullYear();
        const month = dateObj.getMonth();
        const day = dateObj.getDate().toString();
        return day + ' ' + monthNames[month] + ' ' + year.toString() + 'г.';
    
    }

    onChangeFileHandler = e => {
        let files = this.state.files;
        let newFiles = e.target.files
        for(let i = 0; i < newFiles.length; i++) {
            if(files.length < 10) {
                files.push(newFiles[i])
            }
        }
        this.setState( { files: files } )           
    }

    clearFiles = () => {
        this.setState({ files: [] })
    }

    render() {
        const { files, comment, pluses, minuses, refunds, earning } = this.state;
        const date = this.props.location.state.date;
        return (
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <h1 className = 'day_date'>{this.getNameDate(date)}</h1>
                        <div className='create_day__files_form'>
                            <h3>Загрузите скриншоты сделок:</h3>
                            <input type='file' name='screens' id='file' className='input_file' multiple accept='image/*' onChange={this.onChangeFileHandler} />
                            <label htmlFor="file">{files.length === 0 ? 'Выберите файлы' : `файлов выбрано : ${files.length}`}</label>
                            <AwesomeButton value='Очистить' onClick={this.clearFiles}/>
                        </div>
                        <AwesomeTextArea 
                            title= 'Введите ваш комментарий:'
                            value = { comment }
                            onChange = { (e) => { this.setState( { comment: e.target.value } ) } }
                        />
                    </div>
                    <div className='content__right'>
                        <div className='create_statistics'>
                            <InputField 
                                className='create_statistics_input'
                                label = 'введите количество плюсов:'
                                type = 'number'
                                value = { pluses }
                                onChange = { (e) => { this.setState( { pluses: e.target.value } ) } }
                            />
                            <InputField 
                                className='create_statistics_input'
                                label = 'введите количество минусов:'
                                type = 'number'
                                value = { minuses }
                                onChange = { (e) => { this.setState( { minuses: e.target.value } ) } }
                            />
                            <InputField 
                                className='create_statistics_input'
                                label = 'введите количество возвратов:'
                                type = 'number'
                                value = { refunds }
                                onChange = { (e) => { this.setState( { refunds: e.target.value } ) } }
                            />
                            <InputField 
                                label = 'введите прибыль / убыток(вводите со знаком минус):'
                                type = 'number'
                                value = { earning }
                                onChange = { (e) => { this.setState( { earning: e.target.value } ) } }
                            />
                            <AwesomeButton 
                            className = 'create_stat_button'
                            value = 'Сохранить'
                            onClick = {this.saveHandler}
                        /> 
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    saveDiaryDay: (data) => {
        dispatch(saveDiaryDay(data));
    }
})

const CreateDiaryDayPageRoute = withRouter(CreateDiaryDayPage);
export default connect(undefined, mapDispatchToProps)(CreateDiaryDayPageRoute);
