import React, { Component } from 'react';
import { Menu } from '../components/Menu';
import { Instruction } from "../components/Instruction";
import ProfitForm from '../components/ProfitForm';

import '../assets/css/Page.css';

export class ProfitFixedAmountPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sum: 0
        }
    }
    sumChangeHandler = (e) => {
        this.setState({sum: e.target.value})
    }
    render() {
        return(
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <ProfitForm/>
                    </div>
                    <div className='content__right'>
                        <Instruction 
                            title='Инструкция'
                        />
                        <ul className = 'instruction_list'>
                            <li>Введите предпологаемую сумму сделки</li>
                            <li>Введите предпологаемое количество сделок в течение одного дня</li>
                            <li>Введите ваш процент прибыльных сделок</li>
                            <li>Введите промежуток торговли, предварительно выбрав единицу времени</li>
                            <li>Жмите "Посчитать"</li>
                        </ul>
                    </div>
                </div>                
            </>
        );
    }
}