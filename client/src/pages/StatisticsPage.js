import React, { Component } from 'react';
import { Menu } from '../components/Menu';
import StatisticsForm  from "../components/StatisticsForm";
import StatisticsTable from '../components/StatisticsTable';
import { Instruction } from "../components/Instruction";
import '../assets/css/Page.css'

export class StatisticsPage extends Component {
    
    render() {
        return(
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <StatisticsForm/>
                        <StatisticsTable/>
                    </div>
                    <div className='content__right'>
                        <Instruction 
                        title = 'Инструкция по вставке:' 
                        />
                        <ul className = 'instruction_list'>
                            <li>Выберите брокера</li>
                            <li>Скопируйте список ваших сделок</li>
                            <li>Вставляйте в отведенное поле</li>
                            <li>Жмите "Посчитать"</li>
                        </ul>
                    </div>
                </div>
            </>
        );
    }
}