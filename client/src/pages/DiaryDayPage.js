import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { removeDay, getDiaryDay, updateDay, removeScreen, addScreens } from '../store/main/actions';
import { Menu } from '../components/Menu';
import { AwesomeImg } from '../components/AwesomeImg';
import { AwesomeButton } from '../components/AwesomeButton';
import { AwesomeTextArea } from '../components/AwesomeTextArea';
import { StatList } from '../components/StatList';
import { InputField } from '../components/InputField';
import '../assets/css/DiaryDayPage.css';
import config from '../config';

const monthNames = [
    'Января',
    'Февраля',
    'Марта',
    'Апреля',
    'Мая',
    'Июня',
    'Июля',
    'Августа',
    'Сентября',
    'Октября',
    'Ноября',
    'Декабря'
];

class DiaryDayPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            pluses: 0,
            minuses: 0,
            refunds: 0,
            earning: 0,
            comment: '',
            updateStat: false,
            updateComm: false
        }
    }

    componentDidMount() {
        this.props.getDay(this.props.location.state.day._id, (day) => {
            const { comment, pluses, minuses, refunds, earning } = day;
            this.setState({
                comment: comment,
                pluses: pluses,
                minuses: minuses,
                refunds: refunds,
                earning: earning
            })
        });
    }


    getNameDate = (date) => {
        const dateObj = new Date(date);
        const year = dateObj.getFullYear();
        const month = dateObj.getMonth();
        const day = dateObj.getDate().toString();
        return day + ' ' + monthNames[month] + ' ' + year.toString() + 'г.';
    
    }

    removeHandler = (day_id) => {
        this.props.removeDay(day_id);
        this.props.history.push('/diary');
    }

    updateHandler = (field) => {
        if(this.state[field]) {
            const { comment, pluses, minuses, refunds, earning } = this.state;
            this.props.updateDay({day_id: this.props.location.state.day._id, comment, pluses, minuses,refunds,earning}, (day) => {
                this.setState({
                    comment: day.comment,
                    pluses: day.pluses,
                    minuses: day.minuses,
                    refunds: day.refunds,
                    earning: day.earning,
                    [field] : false
                })
            })
        }
        else {
            this.setState({[field]: true})
        }
    }

    canselUpdate = (field) => {
        this.props.getDay(this.props.location.state.day._id, (day) => {
            const { comment, pluses, minuses, refunds, earning } = day;
            this.setState({
                comment: comment,
                pluses: pluses,
                minuses: minuses,
                refunds: refunds,
                earning: earning,
                [field]: false
            })
        });
    }

    removeScreenHandler = (screen_id) => {
        this.props.removeScreen({screen_id: screen_id, day_id: this.props.day._id});
    }

    onChangeFileHandler = e => {
        let files = this.state.files;
        let newFiles = e.target.files
        for(let i = 0; i < newFiles.length; i++) {
            if(files.length < 10 && this.props.day.screens.length < 20) {
                files.push(newFiles[i])
            }
        }
        this.setState( { files: files } )           
    }

    sendScreens = () => {
        const { files } = this.state;
        const data = new FormData();
        files.forEach(file => {
            data.append('screens', file);
        })
        data.append('day_id', this.props.day._id);
        this.setState({files: []});
        this.props.addScreens(data);
    }

    render() {
        const { _id, date, comment } = this.props.day;
        const screens = this.props.day.screens ? this.props.day.screens : [];
        const { pluses, minuses, refunds, earning, files } = this.state;
        let key = 1;
        return (
            <>
                <Menu/>
                <div className = 'content'>
                    <div className = 'content__left'>
                        <h1 className = 'day_date'>{this.getNameDate(date)}</h1>
                        <div className='screens'>
                            {
                                screens.map(screen => 
                                    <div className='day_screen' key={key++}>
                                        <AwesomeImg className='screens_img'  src={config.urlServer + '/' + screen.path}  alt=''/>
                                        <div onClick={() => this.removeScreenHandler(screen._id)}  className='day_screen_rem_but' >&#215;</div>
                                    </div>
                                )
                            }
                        </div>
                        <div className='day_files_form'>
                            <input type='file' name='screens' id='file' className='input_file' multiple accept='image/*' onChange={this.onChangeFileHandler} />
                            <label htmlFor="file">{files.length === 0 ? 'Выберите файлы' : `файлов выбрано : ${files.length}`}</label>
                            <AwesomeButton value='Очистить' onClick={() => this.setState({files: []})}/>
                            <AwesomeButton value='Отправить' className='day_files_form_but' onClick={this.sendScreens}/>
                        </div>
                        {
                            !this.state.updateComm ? 
                            <div className='day_comment'>{comment ? comment : 'Нет комментария.'}</div> :
                            <AwesomeTextArea 
                                title= 'Введите ваш комментарий:'
                                value = { this.state.comment }
                                onChange = { (e) => { this.setState( { comment: e.target.value } ) } }
                            />
                        }
                        <AwesomeButton 
                            className = 'day_update_but'
                            value = {this.state.updateComm ? 'сохранить' : 'редактировать' }
                            onClick = { () => this.updateHandler('updateComm') } 
                        />
                        {
                                this.state.updateComm ? 
                                <AwesomeButton 
                                    className = 'day_update_but'
                                    value = 'отменить' 
                                    onClick = {() => this.canselUpdate('updateComm')} 
                                /> : null
                            } 
                    </div>
                    <div className = 'content__right'>
                        <div className = 'day_statistics'>
                            { this.state.updateStat ? 
                                <div className='create_statistics'>
                                    <InputField 
                                        className='create_statistics_input'
                                        label = 'введите количество плюсов:'
                                        type = 'number'
                                        value = { pluses }
                                        onChange = { (e) => { this.setState( { pluses: e.target.value } ) } }
                                    />
                                    <InputField 
                                        className='create_statistics_input'
                                        label = 'введите количество минусов:'
                                        type = 'number'
                                        value = { minuses }
                                        onChange = { (e) => { this.setState( { minuses: e.target.value } ) } }
                                    />
                                    <InputField 
                                        className='create_statistics_input'
                                        label = 'введите количество возвратов:'
                                        type = 'number'
                                        value = { refunds }
                                        onChange = { (e) => { this.setState( { refunds: e.target.value } ) } }
                                    />
                                    <InputField 
                                        label = 'введите прибыль / убыток(вводите со знаком минус):'
                                        type = 'number'
                                        value = { earning }
                                        onChange = { (e) => { this.setState( { earning: e.target.value } ) } }
                                    />
                                </div> :
                                <StatList stat = {this.props.day}/>
                            }
                            <AwesomeButton 
                                className = 'day_update_but'
                                value = {this.state.updateStat ? 'сохранить' : 'редактировать' }
                                onClick = { () => this.updateHandler('updateStat') } 
                            />
                            {
                                this.state.updateStat ? 
                                <AwesomeButton 
                                    className = 'day_update_but'
                                    value = 'отменить' 
                                    onClick = {() => this.canselUpdate('updateStat')} 
                                /> : null
                            }


                            <AwesomeButton 
                                className = 'day_remove_but'
                                value = 'удалить' 
                                onClick = {() => { this.removeHandler(_id) } } 
                            />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = state => ({
    day: state.diary.day
})

const mapDispatchToProps = dispatch => ({
    removeDay: day_id => {
        dispatch(removeDay(day_id));
    },
    getDay: (day_id, cb) => {
        dispatch(getDiaryDay(day_id, cb))
    },
    updateDay: (data, cb) => {
        dispatch(updateDay(data, cb))
    },
    removeScreen: (data) => {
        dispatch(removeScreen(data))
    },
    addScreens: (data) => {
        dispatch(addScreens(data))
    }
})

 const DiaryDayPageRoute = withRouter(DiaryDayPage);

 export default connect(mapStateToProps, mapDispatchToProps)(DiaryDayPageRoute);