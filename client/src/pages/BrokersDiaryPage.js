import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDiary } from '../store/main/actions';
import { Menu } from '../components/Menu';
import DayPicker from '../components/DayPicker/DayPicker';
import { BigDate } from '../components/BigDate';
import { withRouter } from 'react-router-dom';
import { InputDate } from '../components/InputDate';
import { AwesomeButton } from '../components/AwesomeButton';
import { StatList } from '../components/StatList';
import '../assets/css/BrokersDiaryPage.css'

const monthNames = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];
const longDayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда' , 'Четвег', 'Пятница', 'Суббота'];
const shortDayNames = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб']

class BrokersDiaryPage extends Component {
    constructor(props) {
        super(props);
        this.activeDates = [];
        this.state = {
            isCorrectStart: false,
            isCorrectEnd: false,
            start: '',
            end: '',
            statistics: {
                pluses: 0,
                minuses: 0,
                refunds: 0,
                earning: 0,
                percent: 0
            }
        }
    }

    componentDidMount() {
        this.props.getDiary();
    }

    bigDateClickHandler = (day) => {
        this.props.history.push('/diaryDay', {day: day} );
    }

    getArrayDates = () => {
        const days = this.props.days;
        const dates = [];
        days.forEach(day => {
            dates.push(new Date(day.date) );
        });
        this.activeDates = dates;
        return dates;
    }

    onDayClickHandler = (day) => {
        const isActive = this.props.days.find(elem => {
            
            return elem.date.getTime() === day.getTime();
        });
        if(!isActive) {
            this.props.history.push('/createDiaryDay', { date: day });
        }
        else {
            this.props.history.push('/diaryDay', { day: isActive });
        }
    }

    startHandler = (date) => {
        let isCorrect = false;
        const isCorrectValue = date.match(/^\d{1,2}\.\d{1,2}\.\d{4}$/);
        if(isCorrectValue) isCorrect = true;

        this.setState( { start: date, isCorrectStart: isCorrect } )
        
    }

    endHandler = (date) => {
        let isCorrect = false;
        const isCorrectValue = date.match(/^\d{1,2}\.\d{1,2}\.\d{4}$/);
        if(isCorrectValue) isCorrect = true;

        this.setState( { end: date, isCorrectEnd: isCorrect } )
        
    }

    getDates = () => {
        const start = this.state.start.split('.');
        const end = this.state.end.split('.');
        return {
            start: `${start[1]}.${start[0]}.${start[2]}`,
            end: `${end[1]}.${end[0]}.${end[2]}`
        }
    }

    calculateStatistics = () => {
        const {start, end} = this.getDates();
        const days = this.props.days;
        const statistics =  {
            pluses: 0,
            minuses: 0,
            refunds: 0,
            earning: 0,
            percent: 0
        };
        try {
            const startDate = new Date(start);
            const endDate = new Date(end);
            if(startDate > endDate) throw new Error('fuck u')
            let segmentDays = days.filter(elem => elem.date >= startDate && elem.date <= endDate)
            segmentDays.forEach(elem => {
                statistics.pluses += +elem.pluses;
                statistics.minuses += +elem.minuses;
                statistics.refunds += +elem.refunds;
                statistics.earning += +elem.earning
            })
            const onePercent = statistics.pluses / ( (statistics.pluses + statistics.minuses + statistics.refunds) / 100 );
            statistics.percent = onePercent ? +(onePercent.toFixed(2)) : 0;
            this.setState( { statistics: statistics } )
        } catch (error) {
            console.log(error);
        }
    }
    
    render() {
        if(this.props.loading) return <Menu/>
        if(this.props.isAuth) {
            return(
                <>
                    <Menu/>
                    <div className='content'>
                        <div className='content__left'>
                            {this.props.days.reverse().slice(0, 6).map(day => <BigDate 
                                key={day._id} 
                                day={day} 
                                onClick={ () => { this.bigDateClickHandler( day ) } }
                            />)}
                            <div className='day_picker_container'>
                                <DayPicker
                                    monthNames={monthNames}
                                    longDayNames={longDayNames}
                                    shortDayNames={shortDayNames}
                                    onDayClick={ this.onDayClickHandler }
                                    active={this.getArrayDates()}
                                />
                            </div>
                        </div>
                        <div className='content__right brokers_page'>
                            <div className = 'inputs_diary'>
                                <InputDate
                                    label = 'дата начала (ДД.ММ.ГГГГ)' 
                                    error = {!this.state.isCorrectStart} 
                                    onChange = {(date) => this.startHandler(date)}
                                    value = { this.state.start }
                                />
                                <InputDate 
                                    label = 'дата конца (ДД.ММ.ГГГГ)' 
                                    error = {!this.state.isCorrectEnd} 
                                    onChange = {(date) => this.endHandler(date)}
                                    value = {this.state.end}
                                />
                            </div>
                            <AwesomeButton 
                                className = 'calcul_diary_but' 
                                value='подсчитать' 
                                onClick = {() => {if(this.state.isCorrectStart && this.state.isCorrectEnd) this.calculateStatistics()}}
                            />
                            <div className='segment_stat'>
                                <StatList stat={this.state.statistics} withPercent={true}/>
                            </div>
                        </div>
                    </div>               
                </>
            );
        }
        else {
            return (
                <>
                    <Menu/>
                    <div className='content'>
                        <h1 className='content__message'>Для доступа к дневнику трейдера необходимо зарегистрироваться или авторизоваться.</h1>
                    </div>
                </>
            )
        }
    }
}

const mapStateToProps = state => ({
    isAuth: state.auth.isAuth,
    days: state.diary.days,
    loading: state.diary.loading
})

const mapDispatchToProps = dispatch => ({
    getDiary: () => {
        dispatch( getDiary() );
    }
})

const BrokersDiaryPageRoute = withRouter(BrokersDiaryPage)
export default connect(mapStateToProps, mapDispatchToProps)(BrokersDiaryPageRoute);