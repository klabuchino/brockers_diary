import React, { Component } from 'react';
import { Menu } from '../components/Menu';
import AuthForm from '../components/AuthForm';



export class AuthenticatePage extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }


    render() {
        return(
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <AuthForm history={this.props.history}/> 
                    </div>
                    <div className='content__right'>
                        
                    </div>

                </div>
                               
            </>
        )
    }
}