import React, { Component } from 'react';
import { Menu } from '../components/Menu';
import { InputField } from '../components/InputField';
import { AwesomeButton } from '../components/AwesomeButton';
import '../assets/css/UpdatePasswordPage.css';
import { postRequest } from '../store/main/requests';
import { withRouter } from 'react-router-dom';
import { Message } from '../components/Message';



class UpdatePasswordPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            isValidEmail: false,
            error: ''
        }
    }

    onChangeEmail = (e) => {
        let isValidEmail = false;
        // eslint-disable-next-line
        if(e.target.value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            isValidEmail = true;
        }
        this.setState( { email: e.target.value, isValidEmail: isValidEmail } )
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.error && prevState.error !== this.state.error) {
            this.timerId = setTimeout(() => {this.setState({ error: '' })}, 3000);
        }       
    }

    sendHandler = async () => {
        let res = await postRequest('/users/updatePassword', { email: this.state.email });
        if(res.error) {
            this.setState({ error: res.error })
        }
        else {
            this.props.history.push('/auth');
        }
    }


    render() {
        return(
            <>
                <Menu/>
                <div className='content'>
                    <div className='content__left'>
                        <div className='upd_pass_form'>
                            <div className='header'>Смена пароля</div>
                            <InputField error={!this.isValidEmail} label = 'введите email, и мы отправим вам новый пароль' onChange = {(e) => this.onChangeEmail(e)} value = { this.state.email}/>
                            <AwesomeButton className='upd_pass_form__but' value = 'отправить' onClick = { this.sendHandler }/>
                        </div>
                    </div>
                    <div className='content__right'>
                        
                    </div>

                </div>
                <Message 
                    value={this.state.error}
                    error={true}
                />        
            </>
        )
    }
}

export default withRouter(UpdatePasswordPage);