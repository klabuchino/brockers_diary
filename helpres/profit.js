function calculateEarnings(sum, amount, percent, gapAmount, gap) {
    let fullGap;
    console.log(gapAmount)
    switch(gap) {
        case 'days':
            fullGap = gapAmount;
            break;
        case 'months':
            fullGap = gapAmount * 30;
            break;
        case 'years':
            fullGap = gapAmount  * 365;
            break;
    }
    const allBet = fullGap * amount;
    const profitable = +(allBet / 100 * percent).toFixed();
    const loss = (allBet - profitable) * sum;
    const profit = (sum * 0.8) * profitable;
    
    return profit - loss;
}

module.exports = calculateEarnings;

