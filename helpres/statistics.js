function parser(str, regExp1, regExp2) {
    const str_bet_and_wins = str.match(regExp1);

    if(!str_bet_and_wins) return {error: 'Не удалось вычислить статистику'}

    let result = [];
    let num_bet_and_win = [];
    for(const bet_and_win of str_bet_and_wins) {

        num_bet_and_win = bet_and_win.match(regExp2).map(elem => Number(elem.replace(',','.')))
        result.push(num_bet_and_win);
    }
    return result;
}

function calculateStatistics(bets, isBinomo) {
    const statistics = {
        pluses: 0,
        minuses: 0,
        refunds: 0,
        earnings: 0,
        percentProfit: 0
    }
    let profit = 0;
    bets.forEach(element => {
        if (isBinomo) {
            profit = element[0] - element[1];
        } 
        else {
            profit = element[1] - element[0];
        }
        
        statistics.earnings += profit;

        if(profit > 0) ++statistics.pluses;
        else if(profit < 0) ++statistics.minuses;
        else ++statistics.refunds;
    });
    statistics.earnings = +statistics.earnings.toFixed(2);
    const allBets = statistics.pluses + statistics.minuses + statistics.refunds;
    statistics.percentProfit = +(statistics.pluses / (allBets / 100) ).toFixed(2);

    return statistics;
}

function getStatistics(text, nameTrade) {
    let bets;
    let isBinomo = false;
    console.log(nameTrade)
    switch(nameTrade) {
        case 'olympTrade': {
            bets = parser(text, /\d+,\d+\s\d+,\d+/g, /\d+,\d+/g);
            break;
        }
        case 'binomo': {
            bets = parser(text, /\d+,\d+\s\$\s+\d+,\d+\s\$/g, /\d+,\d+/g);
            isBinomo = true;
            break;
        }
        case 'intradeBar': {
            bets = parser(text,  /\s+\d+([.,]?\d+)?\s+[₽$]\s+\d+([.,]?\d+)?\s+[₽$]/g, /\d+([.,]?\d+)?/g);
            break;
        }
        default: {
            bets = {
                error: 'Выберите площадку'
            };
        }
    }
    if(bets.error) 
        return bets;
    return calculateStatistics(bets, isBinomo);
}

module.exports = getStatistics;
